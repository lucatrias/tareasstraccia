def suma(numeroUno, numeroDos) :
    suma = numeroUno + numeroDos
    return suma

def resta(numeroUno, numeroDos) :
    resta = numeroUno - numeroDos
    return resta

def multiplicacion(numeroUno, numeroDos) :
    multi = numeroUno * numeroDos
    return multi

def division(numeroUno, numeroDos) :
    if numeroDos == 0 :
        return "No se puede dividir por 0 :("

    div = numeroUno / numeroDos
    return div 
    
def operacion(numeroUno, numeroDos, operador):

    cuenta = 0

    if operador == "+" :
        cuenta = suma(numeroUno, numeroDos)
    elif operador == "-":
        cuenta = resta(numeroUno, numeroDos)
    elif operador == "*":
        cuenta = multiplicacion(numeroUno, numeroDos)
    elif operador == "/":
        cuenta = division(numeroUno, numeroDos)
    else:
        cuenta = "error"
    
    return cuenta

print("Ingrese numero 1")
numeroUno = float(input())
print("Ingrese operador")
operador = str(input())
print("Ingrese numero 2")
numeroDos = float(input())

print("Resultado: " + str(operacion(numeroUno, numeroDos, operador)))


#LOPEZ BARCO, TAPIA, TRIAS